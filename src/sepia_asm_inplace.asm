%define COLOR_SIZE 1

%macro convert_pixels_to_float 2
    xor rax, rax
    mov al, %1
    shl rax, 32
    mov al, %1
    movd mm0, rax
    cvtpi2ps %2, mm0
    movlhps %2, %2
%endmacro

section .rodata

column1: dd 0.131, 0.168, 0.189, 0
column2: dd 0.534, 0.686, 0.769, 0
column3: dd 0.272, 0.349, 0.393, 0

section .text
global sepia_asm

sepia_asm:
        mov r8, [rdi + COLOR_SIZE*3]    ;буфер
        convert_pixels_to_float [rdi], xmm0
        convert_pixels_to_float [rdi + COLOR_SIZE], xmm1
        convert_pixels_to_float [rdi + COLOR_SIZE*2], xmm2

        movups xmm3, [column1]
        movups xmm4, [column2]
        movups xmm5, [column3]

        mulps xmm0, xmm3
        mulps xmm1, xmm4
        mulps xmm2, xmm5
        addps xmm0, xmm1
        addps xmm0, xmm2

        cvtps2dq xmm0, xmm0
        packusdw xmm0, xmm0
        packuswb xmm0, xmm0

        movd [rdi], xmm0
        mov [rdi + COLOR_SIZE*3], r8    ;поле b следующего пикселя меняется, из-за того, что вставляются 4 элемента. Исправляю это
        ret