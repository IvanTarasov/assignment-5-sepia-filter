#include "../include/image.h"
#include <stdio.h>

struct image create_image(uint64_t height, uint64_t width) {
    struct image new = {
            .height = height,
            .width = width,
            .data = malloc((sizeof (struct pixel)) * height * width + 1)
    };
    return new;
}

inline uint32_t get_index(struct image* image, uint32_t i, uint32_t j) {
    return i * image->width + j;
}

inline void image_destroy(struct image* image) {
    free(image->data);
}

inline struct pixel* get_pixel(struct image* image, uint32_t i, uint32_t j) {
    return &image->data[get_index(image, i, j)];
}

void set_pixel(struct image* image, struct pixel* pixel, uint32_t i, uint32_t j) {
    image->data[get_index(image, i, j)].b = pixel->b;
    image->data[get_index(image, i, j)].g = pixel->g;
    image->data[get_index(image, i, j)].r = pixel->r;
}

void print_pixels(struct image* image) {
    for (uint64_t i = 0; i < image->height; i++) {
        printf("\n%zu: ", i + 1);
        for (uint64_t j = 0; j < image->width; j++) {
            uint32_t index = get_index(image, i, j);
            printf("_%u_%02x%02x%02x ", index, image->data[index].b, image->data[index].g, image->data[index].r);
        }
    }
}
