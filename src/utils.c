#include "../include/utils.h"

#define ARGS_COUNT_MIN 2

const char* io_errors[5] = {
        "",
        "Could not read pixel!",
        "Non-24 bit BMP files are not supported!",
        "Could not read BMP header!",
        "You have opened not a BMP file!",
};
int error_read_check(enum read_status readStatus) {
    if (readStatus == READ_OK)
        return 1;
    printf("%s", io_errors[readStatus]);
    return 0;
}

int error_write_check(enum write_status writeStatus) {
    if (writeStatus == WRITE_ERROR) {
        fprintf(stderr, "Could not write data to BMP file!");
        return 0;
    }
    return 1;
}

void arguments_count(int argc) {
    if (argc >= ARGS_COUNT_MIN) {
        return;
    }
    fprintf(stderr, "Usage: ./image-transformer <source-image> <transformed-image> <angle>\n");
    exit(1);
}

FILE* file_open(char* file_name, char* mode) {
    FILE* file = fopen(file_name, mode);
    if (file == NULL) {
        fprintf(stderr, "Could not open file \"%s\"! Check file name, path or its rights.", file_name);
        exit(1);
    }
    return file;
}

void file_close(FILE* file) {
    if (fclose(file) != 0) {
        fprintf(stderr, "Could not close file!");
        exit(1);
    }
}
