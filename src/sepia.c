#include "../include/image.h"
#include <stdio.h>


extern void sepia_asm(struct pixel*);

static unsigned char sat( uint64_t x) {
    if (x < 256) return x; return 255;
}

static void sepia_one_pixel( struct pixel* const pixel) {
    static const float c[3][3] = {
            { .131f, .543f, .272f },
            { .168f, .686f, .349f },
            { .189f, .769f, .393f } };

    struct pixel const old = *pixel;
    pixel->b = sat(old.b * c[0][0] + old.g * c[0][1] + old.r * c[0][2]);
    pixel->g = sat(old.b * c[1][0] + old.g * c[1][1] + old.r * c[1][2]);
    pixel->r = sat(old.b * c[2][0] + old.g * c[2][1] + old.r * c[2][2]);

}

void sepia_c_inplace( struct image* img ) {
    for( uint32_t i = 0; i < img->height; i++ ) {
        for (uint32_t j = 0; j < img->width; j++) {
            struct pixel* pixel = get_pixel(img, i, j);
            sepia_one_pixel(pixel);
            set_pixel(img, pixel, i, j);
        }
    }
}

void sepia_asm_inplace( struct image* img ) {
    for( uint32_t i = 0; i < img->height; i++ ) {
        for (uint32_t j = 0; j < img->width; j++) {
            struct pixel* pixel = get_pixel(img, i, j);
            sepia_asm(pixel);
        }
    }
}