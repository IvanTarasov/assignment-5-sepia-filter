#include "../include/bmp.h"
#include <sys/resource.h>
#include "../include/image.h"
#include "../include/sepia.h"

void work(void (*func) (struct image*), char* message, char* args[]) {
    struct rusage r;
    struct timeval start;
    struct timeval end;
    getrusage(RUSAGE_SELF, &r );
    start = r.ru_utime;

    FILE* file = file_open(args[1], "rb");
    struct image image = {0};
    struct bmp_header header = {0};
    if (!error_read_check(from_bmp(file, &header, &image))) {
        file_close(file);
        exit(1);
    }
    file_close(file);

    FILE* file1 = file_open(args[2], "wb");
    func(&image);
    if (!error_write_check(to_bmp(file1, &header, &image))) {
        file_close(file1);
        exit(1);
    }

    image_destroy(&image);
    file_close(file1);

    getrusage(RUSAGE_SELF, &r );
    end = r.ru_utime;
    long res = ((end.tv_sec - start.tv_sec) * 1000000L) +
               end.tv_usec - start.tv_usec;
    printf( "%s: %ldms\n", message, res );
}

int main(int argc, char* args[]) {
    arguments_count(argc);

    work(sepia_c_inplace, "C time", args);
    work(sepia_asm_inplace, "Assembly time", args);
    return 0;
}