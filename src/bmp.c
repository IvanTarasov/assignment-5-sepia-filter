#include "../include/image.h"
#include "bmp.h"

#define BMP_TYPE 19778
#define BI_BIT_COUNT 24

inline static uint8_t get_padding(uint32_t width) {
    return width % 4;
}

static enum read_status convert_to_image(FILE* file, struct bmp_header* header, struct image* image) {
    *image = create_image(header->biHeight, header->biWidth);
    if (image->data == NULL)
        return MEMORY_ALLOCATE_ERROR;

    fseek(file, header->bOffBits, SEEK_SET);
    for (uint32_t i = 0; i < header->biHeight; i++) {
        if (fread(&image->data[i * header->biWidth], sizeof (struct pixel) * header->biWidth, 1, file) != 1) {
            image_destroy(image);
            file_close(file);
            return READ_INVALID_PIXEL;
        }
        if(fseek(file, get_padding(header->biWidth), SEEK_CUR) != 0) {
            image_destroy(image);
            file_close(file);
            return READ_INVALID_PIXEL;
        }
    }
    return READ_OK;
}

void print_bmp(struct bmp_header header) {
    printf("bfType = %d\n", header.bfType);
    printf("bfileSize = %d\n", header.bfileSize);
    printf("bfReserved = %d\n", header.bfReserved);
    printf("bOffBits = %d\n", header.bOffBits);
    printf("biSize = %d\n", header.biSize);
    printf("biWidth = %d\n", header.biWidth);
    printf("biHeight = %d\n", header.biHeight);
    printf("biPlanes = %d\n", header.biPlanes);
    printf("biBitCount = %d\n", header.biBitCount);
    printf("biCompression = %d\n", header.biCompression);
    printf("biSizeImage = %d\n", header.biSizeImage);
    printf("biXPelsPerMeter = %d\n", header.biXPelsPerMeter);
    printf("biYPelsPerMeter = %d\n", header.biYPelsPerMeter);
    printf("biClrUsed = %d\n", header.biClrUsed);
    printf("biClrImportant = %d\n", header.biClrImportant);
}

enum read_status from_bmp(FILE* file, struct bmp_header* header, struct image* image) {
    if (fread(header, sizeof(struct bmp_header), 1, file) != 1) {
        file_close(file);
        return READ_INVALID_HEADER;
    }
    if (header->bfType != BMP_TYPE) {
        file_close(file);
        return READ_INVALID_TYPE;
    }
    if (header->biBitCount != BI_BIT_COUNT) {
        file_close(file);
        return READ_INVALID_BITS;
    }
    return convert_to_image(file, header, image);
}


enum write_status to_bmp(FILE* file, struct bmp_header* header, struct image* image) {
    header->biWidth = image->width;
    header->biHeight = image->height;

    uint8_t padding = get_padding(header->biWidth);
    uint8_t* padding_array[4] = {0};
    if(fwrite(header, sizeof (struct bmp_header), 1, file) != 1 ||
       fseek(file, header->bOffBits, SEEK_SET) != 0) {
        file_close(file);
        image_destroy(image);
        return WRITE_ERROR;
    }

    for (uint32_t i = 0; i < image->height; i++) {
        if (fwrite(&image->data[i * image->width], sizeof(struct pixel) * image->width, 1, file) != 1 ||
            fwrite(padding_array, sizeof (padding), padding, file) != padding) {
            file_close(file);
            image_destroy(image);
            return WRITE_ERROR;
        }
    }
    return WRITE_OK;
}
