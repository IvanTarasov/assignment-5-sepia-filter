#ifndef LAB3_IMAGE_H
#define LAB3_IMAGE_H

#include <stdint.h>
#include <stdlib.h>

struct pixel { uint8_t b, g, r;};

struct image {
    uint64_t width, height;
    struct pixel* data;
};

void image_destroy(struct image* image);
void print_pixels(struct image* image);
struct image create_image(uint64_t height, uint64_t width);
uint32_t get_index(struct image* image, uint32_t i, uint32_t j);
struct pixel* get_pixel(struct image* image, uint32_t i, uint32_t j);
void set_pixel(struct image* image, struct pixel* pixel, uint32_t i, uint32_t j);

#endif //LAB3_IMAGE_H
