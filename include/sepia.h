#ifndef SEPIA_SEPIA_H
#define SEPIA_SEPIA_H
#include "../include/image.h"

void sepia_c_inplace( struct image* img );
void sepia_asm_inplace( struct image* img );

#endif //SEPIA_SEPIA_H
