#ifndef IMAGE_TRANSFORMER_UTILS_H
#define IMAGE_TRANSFORMER_UTILS_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

enum read_status  {
    READ_OK = 0,
    READ_INVALID_PIXEL,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_INVALID_TYPE,
    MEMORY_ALLOCATE_ERROR
};

enum write_status  {
    WRITE_OK = 0,
    WRITE_ERROR
};

void arguments_count(int argc);
int error_read_check(enum read_status readStatus);
int error_write_check(enum write_status writeStatus);
FILE* file_open(char* file_name, char* mode);
void file_close(FILE* file);

#endif
